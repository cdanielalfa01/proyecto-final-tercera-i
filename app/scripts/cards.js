console.log('Cargador Cards...');
const dataCards = [
    {
        "title": "Titulo de bachiller",
        "url_image": "https://i.pinimg.com/564x/16/e6/b4/16e6b4e4cd39081350047bc7e6c436ae.jpg",
        "desc": "Este titulo se me otorgo en el 2018, en ese momento tenia 17 años de edad",
        "cta": "Mostrar mas...",
        "link": "https://es.wikipedia.org/wiki/Educaci%C3%B3n_en_Costa_Rica"
    },
    {
        "title": "Ingeniería en Sistemas",
        "url_image": "https://www.utel.edu.mx/blog/wp-content/uploads/2014/02/shutterstock_148972376.jpg",
        "desc": "Empecé a estudiar en el 2020, en ese momento tenia 19 años de edad",
        "cta": "Mostrar mas...",
        "link": "https://es.wikipedia.org/wiki/Educaci%C3%B3n_en_Costa_Rica"
    },
    {
        "title": "Titulo de bachiller",
        "url_image": "https://i.pinimg.com/564x/16/e6/b4/16e6b4e4cd39081350047bc7e6c436ae.jpg",
        "desc": "Este titulo se me otorgo en el 2018, en ese momento tenia 17 años de edad",
        "cta": "Mostrar mas...",
        "link": "https://es.wikipedia.org/wiki/Educaci%C3%B3n_en_Costa_Rica"
    },
    {
        "title": "Titulo de bachiller",
        "url_image": "https://i.pinimg.com/564x/16/e6/b4/16e6b4e4cd39081350047bc7e6c436ae.jpg",
        "desc": "Este titulo se me otorgo en el 2018, en ese momento tenia 17 años de edad",
        "cta": "Mostrar mas...",
        "link": "https://es.wikipedia.org/wiki/Educaci%C3%B3n_en_Costa_Rica"
    },
    {
        "title": "Titulo de bachiller",
        "url_image": "https://i.pinimg.com/564x/16/e6/b4/16e6b4e4cd39081350047bc7e6c436ae.jpg",
        "desc": "Este titulo se me otorgo en el 2018, en ese momento tenia 17 años de edad",
        "cta": "Mostrar mas...",
        "link": "https://es.wikipedia.org/wiki/Educaci%C3%B3n_en_Costa_Rica"
    },
    {
        "title": "Titulo de bachiller",
        "url_image": "https://i.pinimg.com/564x/16/e6/b4/16e6b4e4cd39081350047bc7e6c436ae.jpg",
        "desc": "Este titulo se me otorgo en el 2019, en ese momento tenia 17 años de edad",
        "cta": "Mostrar mas...",
        "link": "https://es.wikipedia.org/wiki/Educaci%C3%B3n_en_Costa_Rica"
    }
];

(function (){
    let Card = {
        init: function(){
            console.log ('El modulo de carga funciona correctamente');
            let _self = this;
            //Llamamos a las funciones
            this.insertData(_self);
        },
        eventHandler: function (_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');
            for(let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('Evento: ', event);
                    _self.showTab(event.target);
                });
            }
        },
        insertData: function(_self){
            dataCards.map(function(item, index){
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
            });
        },

        tplCardItem: function (item, index) {
            return (`<div class='card-item' id='card-number-${index}'>
            <img src='${item.url_image}'/>
            <div class='card-info'>
            <p class='card-title'>${item.title}</p>
            <p class='card-desc'>${item.desc}</p>
            <a class='card-cta' target='blank' href='${item.link}'>${item.cta}'</a>
            </div>
            </div>`)
        },
    }

    Card.init();
})();