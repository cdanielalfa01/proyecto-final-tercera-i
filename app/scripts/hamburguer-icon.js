console.log('Cargador Hamburger icon...');
(function () {
    const MAIN_OBJ = {
        init: function () {
            this.eventHandlers();
        },
        eventHandlers: function() {
            document.querySelector('.hamburgerIcon').addEventListener('click', function(){
                document.querySelector('.menuContainer').classList.toggle('menuOpen');
            });
        document.querySelector('.subHamburgerIcon').addEventListener('click', function(){
            document.querySelector('.subMenuContainer').classList.toggle('subMenuOpen');
            });
        }
    }
    MAIN_OBJ.init();
})();