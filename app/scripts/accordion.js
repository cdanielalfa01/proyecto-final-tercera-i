console.log('Cargando Accordion...');
const dataAccordion = [
    {"title": "Videojuegos",
    "desc": "¿Cuales son las partes más importantes de un videojuego? Es posible que una gran mayoría de jugadores coincidan en sus respuestas: La jugabilidad o la capacidad de entretener, emocionar y enganchar pueden ser las respuestas más populares. Estoy seguro de que habría debate por otras muchas respuestas. "},
    {"title": "Football",
    "desc": "Hay un número mítico en el fútbol que es indiscutible. El 10 es un referente a nivel internacional desde siempre, como lo demuestra que lo llevasen jugadores como Maradona, Laudrup o Cruyff entre otros. Aunque si hablamos de la Liga Española, y más concretamente del Real Madrid, hay un dorsal que ha marcado a la mayoría de quienes lo han llevado. Esta es la historia del número 7 del Real Madrid, con el que han jugado algunos de los mejores futbolistas del mundo. Actualmente Eden Hazard lo porta, pero en anteriores temporadas lo llevaron jugadores como Cristiano Ronaldo, Emilio Butragueño, Raúl González, entre otras estrellas del equipo blanco. "},
    {"title": "Comida",
    "desc": "La verdad es que no tengo comida favorita, pero siempre me han gustado mucho las hamburguesas, creo que esa será mi comida favorita aunque no estoy 100% seguro."},

];

(function () {
    let Accordion = {
        init: function () {
            let _self = this;
            //llamanos las funciones
            this.insertData(_self);
            this.eventHandler(_self);
        },
        eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function (event) {
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        showTab: function (refItem) {
            let activeTab = document.querySelector('.tab-active');

            if (activeTab) {
                activeTab.classList.remove('tab-active');
            }

            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },

        insertData: function (_self) {
            dataAccordion.map(function (item, index) {
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordion(item));
            });
        },
        tplAccordion: function (item) {
            return (`<div class='accordion-item'>
            <div class='accordion-title'><p>${item.title}</p></div>
            <div class='accordion-desc'><p>${item.desc}</p></div>
            </div>`)
        },

    }

    Accordion.init()
})();